import java.io.*;

/*
*Primera versión del projecto, Entrada/Salida exclusivamente y sin modularidad.
*
* @author Arturo vañó Hernández
*/
public class Proyecto1{

	public static void main(String args[]){

		if(args.length<1){

				System.out.println("Error, No has pasado parámetros.");
			
			}

		else{

			File f = new File(args[0]);

			if (( !f.exists() ) || ( !f.isFile())){

				System.out.println("Error, El fichero indicado no existe o no es un fichero corriente.");
			
			}

			else{

				System.out.println("¿Que grupo prefieres?" );
				System.out.println("a) Metallica");
				System.out.println("b) Led Zeppeling ");
				System.out.println("c) AC/DC");

				String opcion=System.console().readLine();
				String rg=null;

				switch (opcion){
					case "a":
						rg = "Metallica"; break;

					case "b":
						rg = "Led Zeppeling"; break;

					case "c":
						rg = "AC/DC"; break;

					default:
						System.out.println("Opcion desconocida..."); break;

					}
				

				System.out.println("Escoge el mal menor:" );
				System.out.println("1) Dormir durante un año");
				System.out.println("2) Ir desnudo durante un dia normal " );

				opcion= System.console().readLine();
				//int eleccion= Integer.parseInt(System.console().readLine());
				String r=null;

				switch (opcion){
					case "1":
						r = "Dormir durante un año"; break;

					case "2":
						r = "Ir desnudo durante un dia normal"; break;

					default:
						System.out.println("Opcion desconocida..."); break;

					}

				if (rg == null || r == null){

					System.out.println("Alguna de las respuestas no son validas, por favor vuelva a realizar la encuesta.");
					System.exit(0);
				}
				
				try{
					System.out.println("Se va a escribir en el fichero: "+ f.getName() );
					FileWriter fw = new FileWriter(f,true);
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write("Grupo de preferencia: "+ rg+ "\t mal menor: "+ r);

					bw.close(); fw.close();
				}

				catch(IOException e){

					System.err.println(e.getMessage());

				}			

			}
		}
	}
}