import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

/*
*Versión del proyecto final, con swing y modular.
*
* @author Arturo vañó Hernández
*/
public class Proyecto3{

	public static void main(String args[]){

		Dialogos dialogo = new Dialogos();

		dialogo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		dialogo.setVisible(true);

	}
}


class Botones extends JPanel{

	public Botones(String titulo, String[] opciones){//se le pasará las preguntas de la encuesta y las respuestas disponibles
		//Creo los bordes con título (las preguntas) del panel
		setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),titulo));
		//Creo la distribución
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS)); //Le decimos que el contenedor padre es en el que nos encontramos y que la distribución sea vertical.
		
		grupo=new ButtonGroup();

		for (int i=0; i<opciones.length; i++){

			JCheckBox box = new JCheckBox(opciones[i]);
			box.setActionCommand(opciones[i]); //Se le asigna al Box su String.
			add(box); grupo.add(box);
			
			box.setSelected(i==0);

		}
	}

	public String Seleccion(){

		ButtonModel boton=grupo.getSelection();//Almacenamos el JCheckBox seleccionado del grupo.
		String s = boton.getActionCommand(); //Devuelve el String del CheckBox seleccionado.

		return s;

	}

	private ButtonGroup grupo;
}



class Dialogos extends JFrame{

	public Dialogos(){

		setTitle("Encuesta");
		setBounds(500,300,600,450);//eje x,eje y,ancho,alto

		//Construcción del panel superior:
		JPanel jp = new JPanel();

		setLayout(new BorderLayout());

		jp.setLayout(new GridLayout(2,1));
		add(jp, BorderLayout.CENTER);//Le digo que el panel se sitúe en el centro.

		String grupos[]={"Led ledZeppeling", "Metallica", "AC/DC"};
		String males[]={"Dormir durante un año","Ir desnudo durante un dia normal"};

		primera = new Botones("¿Qué grupo prefieres?", grupos);
		jp.add(primera);

		segunda = new Botones("Escoge el mal menor", males);
		jp.add(segunda);

		//Construcción del panel inferior:
		jp2 = new JPanel();
		JButton botonGuardar = new JButton("Guardar");
		JButton botonChooser = new JButton("Escoge fichero");

		//Dotamos a los botones de acciones:
		botonGuardar.addActionListener(new accionMostrar());
		botonChooser.addActionListener(new elegirFichero());

		jfc = new JFileChooser("");

		ruta = new JTextArea("Fichero no elegido");

		jp2.add(botonGuardar); jp2.add(botonChooser); jp2.add(ruta);

		add(jp2,BorderLayout.SOUTH);//Este JPanel se colocará en la zona inferior.

	}

	

	//private class elegirFichero implements ActionListener{}
	private class accionMostrar implements ActionListener{

		public void actionPerformed(ActionEvent e){

			 try{

                   FileWriter fw = new FileWriter(f,true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write("Grupo elegido: " + primera.Seleccion() + ", mal menor: "+ segunda.Seleccion()+ "\n");//Llama al método Seleccion de los objetos tipo Botones para que devuelva la opción de cada grupo seleccionada.
                    bw.close();fw.close();

                }

                catch(IOException ex){
                    System.err.println(ex.getMessage());
                }
		}
	
	}

	private class elegirFichero implements ActionListener{

		public void actionPerformed(ActionEvent e2){
			
			int returnVal = jfc.showOpenDialog(jp2);

                if (returnVal == JFileChooser.APPROVE_OPTION)
                	
                	f = jfc.getSelectedFile();
                    ruta.setText(f.getName()); //Mostraremos un jtextArea que indicará el fichero elegido.*/
		}
	}

	private Botones primera, segunda;//Creo las instancias del objeto Botones fuera del constructor.
	private JFileChooser jfc;
	private File f = null;
	private JTextArea ruta; 
	private JPanel jp2;
}
