import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

/*
*Segunda versión del proyecto, con Swing y Monolítico.
*
* @author Arturo vañó Hernández
*/

public class Proyecto2{

    private static File f = null;
    //Las siguientes declaraciones fuera de main solo son necesarias al compilar con maven.
    private static JFileChooser jfc=null;
    private static ButtonGroup grupo;
    private static ButtonGroup grupo2;
    private static JPanel panelInferior; 
    private static JTextArea ruta;
    
    public static void main(String args[]){
        
        JFrame frame = new JFrame();
        frame.setTitle("Encuesta");
        frame.setBounds(500,300,600,450);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        JPanel jp = new JPanel();

        frame.add(jp,BorderLayout.CENTER);//add(jp);
        jp.setLayout(new GridLayout(2,1));

        //Creación de los bordes:
        jp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),"Elige una opción:"));
        //Establecemos una disposición tipo caja en distribución vertical
        jp.setLayout(new BoxLayout(jp,BoxLayout.Y_AXIS));

        JLabel jLabel1 = new JLabel("Que grupo prefieres:"); jp.add(jLabel1);
        //Creamos dos instacias de ButtonGroup, para que solo pueda permanecer activado un botón del grupo:
          grupo= new ButtonGroup();
          grupo2= new ButtonGroup();

        String grupos[]= {"Led ledZeppeling", "Metallica", "AC/DC" };

        for(int i=0; i < grupos.length; i++){ //bucle para automatizar la tarea.

            JCheckBox b = new JCheckBox(grupos[i]);
            b.setActionCommand(grupos[i]);
            jp.add(b);
            grupo.add(b);
            b.setSelected(i==0);//Para que aparezca la primera opción seleccionada por defecto.

        }
       
       
        JLabel jLabel2 = new JLabel("Que prefieres:"); jp.add(jLabel2);
        
        JRadioButton diaDesn = new JRadioButton("Dormir durante un año");
        JRadioButton dormir = new JRadioButton("Ir desnudo durante un dia normal");
        jp.add(diaDesn);jp.add(dormir);
        grupo2.add(dormir);
        grupo2.add(diaDesn);
        diaDesn.setActionCommand("Ir desnudo durante un dia normal");
        dormir.setActionCommand("Dormir durante un año");
        diaDesn.setSelected(true);

        
        ButtonModel miboton1 = grupo.getSelection();
        String seleccion1 =  miboton1.getActionCommand();//devuelve un string correspondiente al botón seleccionado del grupo.

        ButtonModel miboton2 = grupo2.getSelection();
        String seleccion2 =  miboton2.getActionCommand();

        panelInferior = new JPanel();
        frame.add(panelInferior,BorderLayout.SOUTH);
         jfc = new JFileChooser("");
        JButton botonGuardar = new JButton("Confirmar");
         ruta = new JTextArea("Fichero no elegido"); panelInferior.add(ruta);
        JButton botonChooser = new JButton("Escoge fichero");
        
        panelInferior.add(botonChooser); panelInferior.add(botonGuardar); 
        frame.pack();


        botonGuardar.addActionListener(new ActionListener(){ //Cuando se clique en el botón, escribirá en el fichero elegido
            public void actionPerformed(ActionEvent e){

                try{

                   FileWriter fw = new FileWriter(f,true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write("Grupo elegido: " + grupo.getSelection().getActionCommand() + ", mal menor: "+ grupo2.getSelection().getActionCommand()+ "\n");
                    bw.close();fw.close();
                }
                catch(IOException ex){
                    System.err.println(ex.getMessage());
                }

            }
        });
        //ESCOGER EL FICHERO EN EL QUE ESCRIBIR:
        botonChooser.addActionListener(new ActionListener(){ 

            public void actionPerformed(ActionEvent e){

                int returnVal = jfc.showOpenDialog(panelInferior);

                if (returnVal == JFileChooser.APPROVE_OPTION)
                    f = jfc.getSelectedFile();
                    ruta.setText(f.getName()); //Mostraremos un jtextArea que indicará el fichero elegido.
            }
        });
       

        
    }
}